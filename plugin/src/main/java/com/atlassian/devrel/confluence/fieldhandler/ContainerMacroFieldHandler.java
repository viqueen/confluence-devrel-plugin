package com.atlassian.devrel.confluence.fieldhandler;


import com.atlassian.confluence.plugins.cql.spi.v2searchhelpers.V2SearchQueryWrapper;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ConstantScoreQuery;
import com.atlassian.confluence.search.v2.query.TermQuery;
import com.atlassian.querylang.fields.BaseFieldHandler;
import com.atlassian.querylang.fields.EqualityFieldHandler;
import com.atlassian.querylang.fields.expressiondata.EqualityExpressionData;
import com.atlassian.querylang.fields.expressiondata.SetExpressionData;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.function.Function;

import static com.atlassian.confluence.plugins.cql.spi.v2searchhelpers.V2FieldHandlerHelper.wrapV2Search;
import static com.atlassian.querylang.fields.expressiondata.EqualityExpressionData.Operator.EQUALS;
import static com.atlassian.querylang.fields.expressiondata.EqualityExpressionData.Operator.NOT_EQUALS;
import static com.atlassian.querylang.fields.expressiondata.SetExpressionData.Operator.IN;
import static com.atlassian.querylang.fields.expressiondata.SetExpressionData.Operator.NOT_IN;
import static com.atlassian.devrel.confluence.extractor.ContainerMacroExtractor.CONTAINER_MACROS_FIELD_NAME;
import static java.util.stream.Collectors.toSet;

public class ContainerMacroFieldHandler extends BaseFieldHandler implements EqualityFieldHandler<String, V2SearchQueryWrapper> {

    private static Function<String, SearchQuery> containerMacroQueryProducer = macroName -> new ConstantScoreQuery(new TermQuery(CONTAINER_MACROS_FIELD_NAME, macroName));
    private final Logger logger = LoggerFactory.getLogger(ContainerMacroFieldHandler.class);

    public ContainerMacroFieldHandler() {
        super(CONTAINER_MACROS_FIELD_NAME);
    }

    @Override
    public V2SearchQueryWrapper build(SetExpressionData expressionData, Iterable<String> values) {
        validateSupportedOp(expressionData.getOperator(), Sets.newHashSet(IN, NOT_IN));
        SearchQuery query = joinSingleValueSearchQueries(values, containerMacroQueryProducer);
        return wrapV2Search(query, expressionData);
    }

    @Override
    public V2SearchQueryWrapper build(EqualityExpressionData expressionData, String value) {
        validateSupportedOp(expressionData.getOperator(), Sets.newHashSet(EQUALS, NOT_EQUALS));
        SearchQuery query = containerMacroQueryProducer.apply(value);
        return wrapV2Search(query, expressionData);
    }

    private SearchQuery joinSingleValueSearchQueries(Iterable<String> values, Function<String, SearchQuery> queryProducer) {
        Set<String> valueSet = ImmutableSet.copyOf(values);
        Set<SearchQuery> queries = valueSet.stream().map(queryProducer).collect(toSet());
        if (queries.size() > 1) {
            return BooleanQuery.composeOrQuery(queries);
        }
        return queries.iterator().next();
    }
}
