package com.atlassian.devrel.confluence.extractor;

import com.atlassian.confluence.macro.wiki.UnmigratedBlockWikiMarkupMacro;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.renderer.v2.WikiMarkupParser;
import com.atlassian.renderer.v2.components.MacroTag;
import com.atlassian.renderer.v2.components.WikiContentHandler;
import com.atlassian.renderer.v2.macro.MacroManager;

import java.util.HashSet;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

class MacroCollector implements MacroDefinitionHandler, WikiContentHandler {

    private Set<String> macroNames;
    private MacroManager macroManager;

    public MacroCollector(MacroManager macroManager) {
        this.macroNames = new HashSet<>();
        this.macroManager = checkNotNull(macroManager);
    }

    @Override
    public void handle(MacroDefinition macroDefinition) {
        macroNames.add(macroDefinition.getName());
        if (macroDefinition.getName().equals(UnmigratedBlockWikiMarkupMacro.MACRO_NAME)) {
            processPotentialWikiMacro(macroDefinition.getBodyText());
        }
    }

    @Override
    public void handleMacro(StringBuffer stringBuffer, MacroTag macroTag, String body) {
        macroNames.add(macroTag.command);
        processPotentialWikiMacro(body);
    }

    @Override
    public void handleText(StringBuffer stringBuffer, String s) {
        //Do Nothing
    }

    public Set<String> getMacroNames() {
        return macroNames;
    }

    protected void processPotentialWikiMacro(String wiki) {
        WikiMarkupParser parser = new WikiMarkupParser(macroManager, this);
        parser.parse(wiki);
    }
}
