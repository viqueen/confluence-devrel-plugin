#! /usr/bin/env bash

_with_arguments () {
    count=${1};
    shift;
    if [[ "$#" -lt ${count} ]]; then
        echo "missing arguments, expected at least ${count} but received $#";
        exit 1;
    fi
}

_with_arguments 2 "$@"
version=${1}; shift
browser=${1}; shift

mvn -B verify \
    -Dxvfb.enable=false \
    -Dwebdriver.browser="${browser}:url=http://127.0.0.1:4444" \
    -Dconfluence.version=${version} \
    $@
